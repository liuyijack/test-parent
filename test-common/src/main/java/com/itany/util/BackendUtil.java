package com.itany.util;

public class BackendUtil {

public static Boolean isEmpty(Object object){
    if(null==object ||"".equals(object)){
        return true;
    }
    return false;
}
    public static String paramUtil(String s){
        if(!BackendUtil.isEmpty(s)){
            char[] c = s.toCharArray();
            StringBuffer str=new StringBuffer();
            for (char ct:c
            ) {
                str.append("/"+ct);
            }
            return str.toString();
        }
        return null;
    }
}
