package com.itany.constant;

public interface Constant {
    /**
     * 用户状态1--OK
     */
    public static final String USER_ENABLE = "1";

    /**
     * 用户状态-1--NG
     */
    public static final String USER_DISABLE = "-1";
    /**
     * 供应商状态1--OK
     */
    public static final String SUPPLIER_ENABLE = "1";

    /**
     * 供应商状态-1--NG
     */
    public static final String SUPPLIER_DISABLE = "-1";

    /**
     * 供应商入库审核状态
     * 1--已审核通过
     */
    public static final String EXAMINE_ENABLE = "1";
    /**
     * 供应商入库审核状态
     * 2--已审核未通过
     */
    public static final String EXAMINE_UNABLE = "2";
    /**
     * 供应商入库审核状态
     * -1--未审核
     */
    public static final String EXAMINE_DISABLE = "-1";

    /**
     * book状态
     * 1--Ok
     */
    public static final String BOOK_ENABLE = "1";
    /**
     * book状态
     *2--已审核未通过
     */
    public static final String BOOK_UNABLE = "2";

    /**
     * book状态
     * -1--未审核
     */
    public static final String BOOK_DISABLE = "-1";


    public static final String BOOK_IMG= "BOOKIMG";

    /**
     * 业务逻辑状态码:成功
     */
    public static final String RESPONSE_CODE_SUCCESS = "2001";


    /**
     * 业务逻辑状态码:失败
     */
    public static final String RESPONSE_CODE_FAIL = "2002";

    /**
     * 业务逻辑状态码:请求参数有误
     */
    public static final String RESPONSE_CODE_REQUEST_PARAMETER_ERROR = "2003";

    /**
     * 业务逻辑状态码:登录超时
     */
    public static final String RESPONSE_CODE_LOGIN_TIMEOUT = "2004";

    /**
     * 业务逻辑状态码:没有权限
     */
    public static final String RESPONSE_CODE_NO_PROMISSION = "2005";

}
