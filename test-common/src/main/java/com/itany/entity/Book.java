package com.itany.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

public class Book implements Serializable {

private Integer id;
private String bookname;
private String author;
@JSONField(format = "yyyy-MM-dd")
private Date publishdate;//出版日期
private Double groupprice;//团购价
private Double price;
private String format;//开本
private Integer pagenumber;//页数
private String ISBN;
private String barcode;//条形码
private String layout;//版次
private Integer printingnumber;//印刷次数
private  Integer register;//册数
private Double weight;
private String bookintroduce;//书籍简介
private String authorintroduce;//作者简介
private String catalog;//目录
private Integer typeid;//书籍类型
@JSONField(format = "yyyy-MM-dd HH:mm:ss")
private Date createtime;//入库时间
private Integer pressid;//出版社
private Integer number;//库存数量
private String imgurl;//封面图片
private String flag;
private String recommend;//推荐类型

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookname='" + bookname + '\'' +
                ", author='" + author + '\'' +
                ", publishdate=" + publishdate +
                ", groupprice=" + groupprice +
                ", price=" + price +
                ", format='" + format + '\'' +
                ", pagenumber=" + pagenumber +
                ", ISBN='" + ISBN + '\'' +
                ", barcode='" + barcode + '\'' +
                ", layout='" + layout + '\'' +
                ", printingnumber=" + printingnumber +
                ", register=" + register +
                ", weight=" + weight +
                ", bookintroduce='" + bookintroduce + '\'' +
                ", authorintroduce='" + authorintroduce + '\'' +
                ", catalog='" + catalog + '\'' +
                ", typeid=" + typeid +
                ", createtime=" + createtime +
                ", pressid=" + pressid +
                ", number=" + number +
                ", imgurl='" + imgurl + '\'' +
                ", flag='" + flag + '\'' +
                ", recommend='" + recommend + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublishdate() {
        return publishdate;
    }

    public void setPublishdate(Date publishdate) {
        this.publishdate = publishdate;
    }

    public Double getGroupprice() {
        return groupprice;
    }

    public void setGroupprice(Double groupprice) {
        this.groupprice = groupprice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getPagenumber() {
        return pagenumber;
    }

    public void setPagenumber(Integer pagenumber) {
        this.pagenumber = pagenumber;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public Integer getPrintingnumber() {
        return printingnumber;
    }

    public void setPrintingnumber(Integer printingnumber) {
        this.printingnumber = printingnumber;
    }

    public Integer getRegister() {
        return register;
    }

    public void setRegister(Integer register) {
        this.register = register;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getBookintroduce() {
        return bookintroduce;
    }

    public void setBookintroduce(String bookintroduce) {
        this.bookintroduce = bookintroduce;
    }

    public String getAuthorintroduce() {
        return authorintroduce;
    }

    public void setAuthorintroduce(String authorintroduce) {
        this.authorintroduce = authorintroduce;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getPressid() {
        return pressid;
    }

    public void setPressid(Integer pressid) {
        this.pressid = pressid;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public Book() {
    }

    public Book(Integer id, String bookname, String author, Date publishdate, Double groupprice, Double price, String format, Integer pagenumber, String ISBN, String barcode, String layout, Integer printingnumber, Integer register, Double weight, String bookintroduce, String authorintroduce, String catalog, Integer typeid, Date createtime, Integer pressid, Integer number, String imgurl, String flag, String recommend) {
        this.id = id;
        this.bookname = bookname;
        this.author = author;
        this.publishdate = publishdate;
        this.groupprice = groupprice;
        this.price = price;
        this.format = format;
        this.pagenumber = pagenumber;
        this.ISBN = ISBN;
        this.barcode = barcode;
        this.layout = layout;
        this.printingnumber = printingnumber;
        this.register = register;
        this.weight = weight;
        this.bookintroduce = bookintroduce;
        this.authorintroduce = authorintroduce;
        this.catalog = catalog;
        this.typeid = typeid;
        this.createtime = createtime;
        this.pressid = pressid;
        this.number = number;
        this.imgurl = imgurl;
        this.flag = flag;
        this.recommend = recommend;
    }
}
