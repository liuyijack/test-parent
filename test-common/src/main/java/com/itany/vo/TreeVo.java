package com.itany.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class TreeVo implements Serializable {

    private Integer id;

    private String text;

    private String state;

    private boolean checked;

    private List<TreeVo> children=new ArrayList<TreeVo>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<TreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeVo> children) {
        this.children = children;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
