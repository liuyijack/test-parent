package com.itany.exception;

public class RequsetErrorException extends Exception {
    public RequsetErrorException() {
        super();
    }

    public RequsetErrorException(String message) {
        super(message);
    }

    public RequsetErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequsetErrorException(Throwable cause) {
        super(cause);
    }

    protected RequsetErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
