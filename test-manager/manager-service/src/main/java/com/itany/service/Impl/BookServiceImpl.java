package com.itany.service.Impl;

import com.itany.constant.Constant;
import com.itany.entity.Book;
import com.itany.exception.RequsetErrorException;
import com.itany.mapper.BookDao;
import com.itany.service.BookService;
import com.itany.util.BackendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Service
@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public class BookServiceImpl implements BookService {

    @Autowired(required = false)
    private BookDao bookDao;

    @Override
    public void addBook(String bookname, String author, Date publishdate, Double groupprice, Double price, String format, Integer pagenumber, String ISBN, String barcode, String layout, Integer printingnumber, Integer register, Double weight, String bookintroduce, String authorintroduce, String catalog, Integer typeid, Date createtime, Integer pressid, Integer number, String imgurl, String flag, String recommend) {
        Book book=new Book();
        book.setAuthor(author);book.setAuthorintroduce(authorintroduce);book.setBarcode(barcode);
        book.setBookintroduce(bookintroduce);book.setBookname(bookname);book.setCatalog(catalog);
        book.setCreatetime(createtime);book.setFlag(Constant.BOOK_DISABLE);book.setFormat(format);
        book.setGroupprice(groupprice);book.setImgurl(imgurl);
        book.setISBN(ISBN);book.setLayout(layout);book.setNumber(number);book.setPagenumber(pagenumber);
        book.setPressid(pressid);book.setPrice(price);book.setWeight(weight);book.setTypeid(typeid);
        book.setRegister(register);book.setRecommend(recommend);book.setPublishdate(publishdate);
        book.setPrintingnumber(printingnumber);
        bookDao.insertBook(book);
    }

    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public List<Book> findAll(Map<String, Object> map) {


        List<Book> books = bookDao.selectByCondition(map);

        return books;
    }
    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public Book findById(Integer id) throws RequsetErrorException {
if(BackendUtil.isEmpty(id)){
    throw new RequsetErrorException("id参数有误");
}
        Book book = bookDao.selectById(id);
        return book;
    }
}
