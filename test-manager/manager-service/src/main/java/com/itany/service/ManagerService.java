package com.itany.service;

import com.itany.entity.Manager;
import com.itany.exception.RequsetErrorException;

import java.util.List;

public interface ManagerService {

List<Manager> findManager(String username);

void addManager(String username,String password,Integer supplierid) throws RequsetErrorException;
}
