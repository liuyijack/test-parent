package com.itany.service;

import com.itany.entity.User;
import com.itany.exception.RequsetErrorException;

import java.util.List;

public interface UserService {

    List<User> findUserAll(String username,String flag);

    User findById(Integer id) throws RequsetErrorException;

    void modifyById(Integer id,String username,String phone,String password,String email,String interest) throws RequsetErrorException;

    void changeUserFlagEnable(Integer id) throws RequsetErrorException;

    void changeUserFlagDisable(Integer id) throws RequsetErrorException;
}
