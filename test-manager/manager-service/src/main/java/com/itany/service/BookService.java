package com.itany.service;

import com.itany.entity.Book;
import com.itany.exception.RequsetErrorException;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface BookService {

void addBook(String bookname, String author, Date publishdate, Double groupprice, Double price, String format, Integer pagenumber, String ISBN, String barcode, String layout, Integer printingnumber, Integer register, Double weight, String bookintroduce, String authorintroduce, String catalog, Integer typeid, Date createtime, Integer pressid, Integer number, String imgurl, String flag, String recommend);

List<Book> findAll(Map<String,Object>map);

Book findById(Integer id) throws RequsetErrorException;

}
