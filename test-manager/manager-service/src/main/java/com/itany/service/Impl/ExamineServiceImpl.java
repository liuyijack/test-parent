package com.itany.service.Impl;

import com.itany.constant.Constant;
import com.itany.entity.Book;
import com.itany.entity.Examine;
import com.itany.entity.Supplier;
import com.itany.exception.RequsetErrorException;
import com.itany.mapper.BookDao;
import com.itany.mapper.ExamineDao;
import com.itany.service.ExamineService;
import com.itany.util.BackendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ExamineServiceImpl implements ExamineService {
    @Autowired(required = false)
    private ExamineDao examineDao;
    @Autowired(required = false)
    private BookDao bookDao;

    @Override
    public void addExamine(String bookname, String ISBN, Double price, Integer number, Double groupprice, String layout, Integer register, Double weight, String bookintroduce, String authorintroduce, String imgurl, Supplier supplier) throws RequsetErrorException {
        if (BackendUtil.isEmpty(bookname) || BackendUtil.isEmpty(ISBN) || BackendUtil.isEmpty(price) || BackendUtil.isEmpty(number) ||
                BackendUtil.isEmpty(groupprice) || BackendUtil.isEmpty(layout) || BackendUtil.isEmpty(register) || BackendUtil.isEmpty(weight) ||
                BackendUtil.isEmpty(bookintroduce) || BackendUtil.isEmpty(authorintroduce) || BackendUtil.isEmpty(imgurl)) {
            throw new RequsetErrorException("请求参数有误");
        }
 /*   if(BackendUtil.isEmpty(supplier)){
            throw new RequsetErrorException("请求参数有误");
        }*/
        Book book = new Book();
        Map<String,Object>map=new HashMap<>();
        map.put("ISBN",ISBN);
        List<Book> books = bookDao.selectByCondition(map);
        if(!books.isEmpty()){
          book=books.get(0);
        }else {
            book.setAuthorintroduce(authorintroduce);
            book.setBookintroduce(bookintroduce);
            book.setBookname(bookname);
            book.setCreatetime(new Date());
            book.setFlag(Constant.BOOK_DISABLE);
            book.setGroupprice(groupprice);
            book.setImgurl(imgurl);
            book.setISBN(ISBN);
            book.setNumber(0);
            book.setLayout(layout);
            book.setPrice(price);
            book.setWeight(weight);
            book.setRegister(register);
            bookDao.insertBook(book);
        }
        Examine examine = new Examine();
        examine.setBookid(book.getId());
        examine.setFlag(Constant.EXAMINE_DISABLE);
        examine.setNumber(number);
        examine.setTitle("《"+bookname+"》" + "审核中");
        examine.setInfo("");
    // examine.setSupplierid(supplier.getId());
        examineDao.insertExamine(examine);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public List<Examine> findAll(String bookname, String ISBN) {
        Map<String,Object> map=new HashMap<>();
        if(!BackendUtil.isEmpty(bookname)){
            map.put("title",bookname);
        }
        if(!BackendUtil.isEmpty(ISBN)){
            map.put("ISBN",ISBN);
        }
        List<Examine> examines = examineDao.selectByCondition(map);
        return examines;
    }
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public Examine findById(Integer id) throws RequsetErrorException {
        if(BackendUtil.isEmpty(id)){
            throw new RequsetErrorException("id参数有误");
        }
        Examine examine =examineDao.selectById(id);
        return examine;
    }

    @Override
    public void modifyById(Integer id, String info, String flag) throws RequsetErrorException {
        if(BackendUtil.isEmpty(id)||BackendUtil.isEmpty(info)||BackendUtil.isEmpty(flag)){
            throw new RequsetErrorException("参数有误");
        }
        Examine examine = examineDao.selectById(id);
        if(flag.equals(examine.getFlag())){
            throw new RequsetErrorException("重填审核状态");
        }
        examine.setInfo(info);examine.setFlag(flag);
        examineDao.updateById(examine);
        Book book = bookDao.selectById(examine.getBookid());
        book.setFlag(examine.getFlag());
        if(Constant.EXAMINE_ENABLE.equals(flag)){
            book.setNumber(book.getNumber()+examine.getNumber());
        }
            bookDao.updateById(book);

    }
}
