package com.itany.service.Impl;

import com.itany.constant.Constant;
import com.itany.entity.Supplier;
import com.itany.exception.RequsetErrorException;
import com.itany.mapper.SupplierDao;
import com.itany.service.SupplierService;
import com.itany.util.BackendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public class SupplierServiceImpl implements SupplierService {
    @Autowired(required = false)
    private SupplierDao supplierDao;

    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public List<Supplier> findSupplierAll(String name) {

        Map<String,Object> map=new HashMap<>();
        map.put("name",name);
        List<Supplier> suppliers = supplierDao.selectByCondition(map);
        return suppliers;
    }


    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public Supplier findBySupplierId(Integer id) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id)){
            throw  new RequsetErrorException("请求参数有误");
        }
        Supplier supplier = supplierDao.selectById(id);
        return supplier;
    }

    @Override
    public void modifySupplierById(Integer id,String name, String linkman, String info, String phone ,Date createtime ) throws RequsetErrorException {

        if (BackendUtil.isEmpty(id)||BackendUtil.isEmpty(name)||BackendUtil.isEmpty(linkman)||BackendUtil.isEmpty(info)||BackendUtil.isEmpty(phone)||BackendUtil.isEmpty(createtime)){
            throw  new RequsetErrorException("请求参数有误");
        }
        Supplier s = supplierDao.selectByName(name);
        if(id.equals(s.getId())){
            throw  new RequsetErrorException("存在同名无法修改");
        }
        Supplier supplier = supplierDao.selectById(id);
        supplier.setPhone(phone);supplier.setName(name);supplier.setLinkman(linkman);supplier.setInfo(info);
supplier.setCreatetime(createtime);
        supplierDao.updatesupplier(supplier);

    }

    @Override
    public void changeSupplierEnable(Integer id) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id)){
            throw  new RequsetErrorException("请求参数有误");
        }
        supplierDao.exchangeflagEnable(id);
    }

    @Override
    public void changeSupplierDisable(Integer id) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id)){
            throw  new RequsetErrorException("请求参数有误");
        }
        supplierDao.exchangeflagDisable(id);
    }

    @Override
    public void addSupplier(String name, String linkman, String info, String phone,Date createtime) throws RequsetErrorException {
        if (BackendUtil.isEmpty(name)||BackendUtil.isEmpty(linkman)||BackendUtil.isEmpty(info)||BackendUtil.isEmpty(phone)||BackendUtil.isEmpty(createtime)){
            throw  new RequsetErrorException("请求参数有误");
        }
        Supplier s = supplierDao.selectByName(name);
        if(!BackendUtil.isEmpty(s)){
            throw  new RequsetErrorException("存在同名无法添加");
        }
        Supplier supplier=new Supplier();
        supplier.setCreatetime(createtime);supplier.setFlag(Constant.SUPPLIER_ENABLE);supplier.setInfo(info);supplier.setLinkman(linkman);supplier.setName(name);supplier.setPhone(phone);
        supplierDao.insertsupplier(supplier);

    }
}
