package com.itany.service.Impl;

import com.itany.entity.Manager;
import com.itany.exception.RequsetErrorException;
import com.itany.mapper.ManagerDao;
import com.itany.service.ManagerService;
import com.itany.util.BackendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public class ManagerServiceImpl implements ManagerService {

    @Autowired(required = false)
    private ManagerDao managerDao;
    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public List<Manager> findManager(String username) {

        Map<String,Object> map=new HashMap<>();
        if(!BackendUtil.isEmpty(username)){
            map.put("username",username);
        }
        List<Manager> managers = managerDao.selectByCondition(map);
        return managers;
    }

    @Override
    public void addManager(String username, String password, Integer supplierid) throws RequsetErrorException {
        if(BackendUtil.isEmpty(username)||BackendUtil.isEmpty(password)||BackendUtil.isEmpty(supplierid)){
          throw new RequsetErrorException("请求参数有误");
        }
        Manager manager=new Manager();manager.setPassword(password);manager.setSupplierid(supplierid);manager.setUsername(username);
        managerDao.insertManager(manager);
    }
}
