package com.itany.service.Impl;

import com.itany.constant.Constant;
import com.itany.entity.User;
import com.itany.exception.RequsetErrorException;
import com.itany.mapper.UserDao;
import com.itany.service.UserService;
import com.itany.util.BackendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserDao userDao;
    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public List<User> findUserAll(String username,String flag) {
        Map<String,Object> map=new HashMap<>();

        if (!BackendUtil.isEmpty(username))
        {map.put("username",username);}
        if (!BackendUtil.isEmpty(flag))
        {map.put("flag",flag);}
        List<User> users = userDao.selectByCondition(map);
        return users;
    }
    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    @Override
    public User findById(Integer id) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id))
        {throw  new RequsetErrorException("请求参数有误");}

        User user = userDao.selectById(id);
        return user;
    }

    @Override
    public void modifyById(Integer id,String username, String phone, String password, String email, String interest) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id)||BackendUtil.isEmpty(username)||BackendUtil.isEmpty(phone)||BackendUtil.isEmpty(password)||BackendUtil.isEmpty(email)||BackendUtil.isEmpty(interest))
        {throw  new RequsetErrorException("请求参数有误");}
        User user = userDao.selectById(id);
        List<User> users = userDao.selectByusername(username);
        for (User u:users
             ) {
            if(!id.equals(u.getId())){
                throw  new RequsetErrorException("有同名用户无法修改");
            }
        }

        user.setEmail(email);user.setInterest(interest);user.setPassword(password);user.setPhone(phone);user.setUsername(username);
        userDao.updateById(user);
    }

    @Override
    public void changeUserFlagEnable(Integer id) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id))
        {throw  new RequsetErrorException("请求参数有误");}
        User user = userDao.selectById(id);
        if(Constant.USER_ENABLE.equals(user.getFlag())){
            throw  new RequsetErrorException("启用状态无需更改");
        }
        userDao.exchangeUserEnable(id);
    }

    @Override
    public void changeUserFlagDisable(Integer id) throws RequsetErrorException {
        if (BackendUtil.isEmpty(id))
        {throw  new RequsetErrorException("请求参数有误");}
        User user = userDao.selectById(id);
        if(Constant.USER_DISABLE.equals(user.getFlag())){
            throw  new RequsetErrorException("禁用状态无需更改");
        }
        userDao.exchangeUserDisable(id);
    }


}
