package com.itany.service;


import com.itany.entity.Examine;
import com.itany.entity.Supplier;
import com.itany.exception.RequsetErrorException;

import java.util.List;

public interface ExamineService {

void addExamine(String bookname, String ISBN, Double price, Integer number, Double groupprice,
                String layout, Integer register, Double weight, String bookintroduce,
                String authorintroduce, String imgurl, Supplier supplier) throws RequsetErrorException;

List<Examine> findAll(String bookname, String ISBN);

Examine findById(Integer id) throws RequsetErrorException;

void modifyById(Integer id,String info,String flag) throws RequsetErrorException;

}
