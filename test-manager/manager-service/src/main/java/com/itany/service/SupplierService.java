package com.itany.service;

import com.itany.entity.Supplier;
import com.itany.exception.RequsetErrorException;

import java.util.Date;
import java.util.List;

public interface SupplierService {

List<Supplier> findSupplierAll(String name);

Supplier findBySupplierId(Integer id) throws RequsetErrorException;

void modifySupplierById(Integer id, String name, String linkman, String info, String phone, Date createtime) throws RequsetErrorException;

void changeSupplierEnable(Integer id) throws RequsetErrorException;

void changeSupplierDisable(Integer id) throws RequsetErrorException;

void addSupplier(String name,String linkman,String info,String phone,Date createtime) throws RequsetErrorException;

}
