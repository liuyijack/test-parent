package com.itany.controller;

import com.itany.constant.Constant;
import com.itany.entity.Book;
import com.itany.exception.RequsetErrorException;
import com.itany.service.BookService;
import com.itany.vo.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/book")
public class BookController {
@Autowired
    private BookService bookService;

@ResponseBody
@RequestMapping("/findById")
    public ActionResult findById(@RequestParam Integer id){
    ActionResult ar=new ActionResult();
    try {
        Book byId = bookService.findById(id);
        ar.setData(byId);
        ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
        ar.setMsg("OK");
    } catch (RequsetErrorException e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
        ar.setMsg(e.getMessage());
    } catch (Exception e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_FAIL);
        ar.setMsg(e.getMessage());
    }
    return  ar;
}



}
