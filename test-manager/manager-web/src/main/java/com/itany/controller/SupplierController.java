package com.itany.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.Constant;
import com.itany.entity.Supplier;
import com.itany.exception.RequsetErrorException;
import com.itany.service.SupplierService;
import com.itany.util.BackendUtil;
import com.itany.vo.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/supplier")
public class SupplierController {

@Autowired
    private SupplierService supplierService;
@ResponseBody
@RequestMapping("/findAll")
public Map<String, Object> findAll(@RequestParam Integer page,
                                   @RequestParam(name = "name", required = false) String name,
                                   @RequestParam Integer rows) {
    Map<String, Object> map = new HashMap<String, Object>();
    PageHelper.startPage(page, rows);
    List<Supplier> all = supplierService.findSupplierAll(BackendUtil.paramUtil(name));
    PageInfo<Supplier> info = new PageInfo<>(all);
    map.put("total", info.getTotal());
    map.put("rows", info.getList());
    return map;
}
    @ResponseBody
    @RequestMapping("/findById")
    public ActionResult findById(@RequestParam Integer id) {
        ActionResult ar = new ActionResult();

        try {
            Supplier bySupplierId = supplierService.findBySupplierId(id);
            ar.setData(bySupplierId);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }

    @ResponseBody
    @RequestMapping("/modifyById")
    public ActionResult modifyById(@RequestParam Integer id,@RequestParam String name,@RequestParam String linkman,@RequestParam String info,@RequestParam String phone,@RequestParam Date createtime) {
        ActionResult ar = new ActionResult();
        try {
            supplierService.modifySupplierById(id,name,linkman,info,phone,createtime);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }
    @ResponseBody
    @RequestMapping("/changeFlagEnable")
    public ActionResult changeFlagEnable(@RequestParam Integer id) {
        ActionResult ar = new ActionResult();
        try {
            supplierService.changeSupplierEnable(id);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }
    @ResponseBody
    @RequestMapping("/changeFlagDisable")
    public ActionResult changeFlagDisable(@RequestParam Integer id) {
        ActionResult ar = new ActionResult();
        try {
            supplierService.changeSupplierDisable(id);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }



}
