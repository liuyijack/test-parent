package com.itany.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.Constant;
import com.itany.entity.Book;
import com.itany.entity.Examine;
import com.itany.entity.Supplier;
import com.itany.exception.RequsetErrorException;
import com.itany.service.ExamineService;
import com.itany.util.BackendUtil;
import com.itany.vo.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/examine")
public class ExamineController {

@Autowired
    private ExamineService examineService;


@ResponseBody
    @RequestMapping("/findAll")
    public Map<String,Object> findAll(@RequestParam Integer page,
                                      @RequestParam(name = "bookname", required = false) String bookname,
                                      @RequestParam(name = "ISBN", required = false) String ISBN,
                                      @RequestParam Integer rows){
    Map<String,Object> map=new HashMap<>();
    PageHelper.startPage(page, rows);
    System.out.println(bookname);
    List<Examine> all = examineService.findAll(BackendUtil.paramUtil(bookname), ISBN);
    PageInfo<Examine> info = new PageInfo<>(all);
    map.put("total", info.getTotal());
    map.put("rows", info.getList());
    return  map;
}

@ResponseBody
    @RequestMapping("/addExamine")
public ActionResult addExamine(String bookname, String ISBN, Double price, Integer number, Double groupprice,
                               String layout, Integer register, Double weight, String bookintroduce,
                               String authorintroduce, @RequestParam(name = "imgurl",required = false) MultipartFile imgurl, HttpSession session){
    ActionResult ar=new ActionResult();
    System.out.println(bookintroduce);
    System.out.println(imgurl);
    Supplier supplier = (Supplier) session.getAttribute("supplier");
    try {
        if (null==imgurl) {
            ar.setMsg("请选择封面图片");
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            return ar;
        }
        String realPath = session.getServletContext().getRealPath("/");
        String filePath = "/upload/" + System.currentTimeMillis() + imgurl.getOriginalFilename();
        File file = new File(realPath, filePath);
        file.getParentFile().mkdirs();
        imgurl.transferTo(file);
        examineService.addExamine(bookname,ISBN,price,number,groupprice,layout,register,weight,bookintroduce,authorintroduce,filePath,supplier);
    ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
    ar.setMsg("OK");
} catch (RequsetErrorException e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
        ar.setMsg(e.getMessage());
    }
    catch (Exception e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_FAIL);
        ar.setMsg(e.getMessage());
    }

 return  ar;
}
@ResponseBody
@RequestMapping("/findById")
public ActionResult findBuId(@RequestParam Integer id){
        ActionResult ar=new ActionResult();
        try {
            Examine byId = examineService.findById(id);
            ar.setData(byId);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return  ar;
}

@ResponseBody
    @RequestMapping("/doplat")
    public ActionResult doplat(Integer id,String flag,String info ){
    ActionResult ar=new ActionResult();
    try {
        examineService.modifyById(id,info,flag);
        ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
        ar.setMsg("OK");
    } catch (RequsetErrorException e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
        ar.setMsg(e.getMessage());
    } catch (Exception e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_FAIL);
        ar.setMsg(e.getMessage());
    }

    return  ar;
}


}
