package com.itany.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.Constant;
import com.itany.entity.Manager;
import com.itany.exception.RequsetErrorException;
import com.itany.service.ManagerService;
import com.itany.vo.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/manageruser")
public class ManagerController {
    @Autowired
    private ManagerService managerService;


    @ResponseBody
    @RequestMapping("/findAll")
    public Map<String,Object> findAll(@RequestParam Integer page,
                                @RequestParam(name="username",required = false)String username,
                                @RequestParam Integer rows){
        Map<String,Object> map = new HashMap<String, Object>();
        System.out.println("username"+username);
        PageHelper.startPage(page,rows);
        List<Manager> manager = managerService.findManager(username);
        System.out.println(manager.get(0));
        PageInfo<Manager> info = new PageInfo<>(manager);
        map.put("total", info.getTotal());//总数据调试等同select count(*)
        map.put("rows", info.getList());
        return map;
    }

@ResponseBody
    @RequestMapping("/addManager")
    public ActionResult addManager(String username,String password,Integer supplierid){
        ActionResult ar=new ActionResult();
    try {
        managerService.addManager(username,password,supplierid);
        ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
        ar.setMsg("添加成功");
    } catch (RequsetErrorException e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
        ar.setMsg(e.getMessage());
    } catch (Exception e) {
        e.printStackTrace();
        ar.setStatus(Constant.RESPONSE_CODE_FAIL);
        ar.setMsg(e.getMessage());
    }
        return ar;
}


}
