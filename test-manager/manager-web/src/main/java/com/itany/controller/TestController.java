package com.itany.controller;


import com.itany.vo.TreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test")
public class TestController {

	
	@RequestMapping("/findAll")
	@ResponseBody
	public Map<String,Object> findAll(@RequestParam(defaultValue="1")Integer page,
									  @RequestParam(name="username",required = false)String username,
			                          @RequestParam(defaultValue="10")Integer rows){
		
		Map<String,Object> map = new HashMap<String, Object>();
/*		PageInfo<User> info = testService.findUserAll(page, rows);
		map.put("total", 100);//总数据调试等同select count(*)
		map.put("rows", info.getList());*/
		return map;
	}

/*	@RequestMapping("/getSelectData")
	@ResponseBody
	public List<User> getSelectData(){
		PageInfo<User> info = testService.findUserAll(1, 10);
		return info.getList();
	}*/

	@RequestMapping("/getTreeData")
	@ResponseBody
	public List<TreeVo> getTreeData(){
		  List<TreeVo> node1 = new ArrayList<TreeVo>();
          for(int i=0;i<2;i++){
              TreeVo vo = new TreeVo();
			  vo.setId(i+1);
			  vo.setText("一级节点"+(i+1));
			  if(i==0){
				  List<TreeVo> node2 = new ArrayList<TreeVo>();
				  TreeVo vo1 = new TreeVo();
				  vo1.setId(100);
				  vo1.setText("二级节点");
				  vo.getChildren().add(vo1);
			  }
			  node1.add(vo);
		  }

		  return node1;
	}


	@RequestMapping("/getTreeData1")
	@ResponseBody
	public List<TreeVo> getTreeData1(Integer id){
		System.out.println("=================="+id);
		List<TreeVo> node1 = new ArrayList<TreeVo>();
		for(int i=0;i<5;i++){
			TreeVo vo = new TreeVo();
			vo.setId(i+1);
			vo.setState("closed");
			vo.setText("一级节点"+(i+1));
			node1.add(vo);
		}

		return node1;
	}

}
