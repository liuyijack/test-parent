package com.itany.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.Constant;
import com.itany.entity.User;
import com.itany.exception.RequsetErrorException;
import com.itany.service.UserService;
import com.itany.util.BackendUtil;
import com.itany.vo.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping("/findAll")
    public Map<String, Object> findAll(@RequestParam Integer page,
                                       @RequestParam(name = "username", required = false) String username,
                                       @RequestParam(name = "flag", required = false) String flag,
                                       @RequestParam Integer rows) {
        Map<String, Object> map = new HashMap<String, Object>();
        PageHelper.startPage(page, rows);
        List<User> users = userService.findUserAll(BackendUtil.paramUtil(username), flag);
        PageInfo<User> info = new PageInfo<>(users);
        map.put("total", info.getTotal());
        map.put("rows", info.getList());
        return map;
    }

    @ResponseBody
    @RequestMapping("/findById")
    public ActionResult findById(@RequestParam Integer id) {
        ActionResult ar = new ActionResult();

        try {
            User byId = userService.findById(id);
            ar.setData(byId);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }

    @ResponseBody
    @RequestMapping("/modifyById")
    public ActionResult modifyById(@RequestParam Integer id,@RequestParam String username,@RequestParam String password,@RequestParam String phone,@RequestParam String email,@RequestParam String interest) {
        ActionResult ar = new ActionResult();
        try {
            userService.modifyById(id,username,phone,password,email,interest);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }
    @ResponseBody
    @RequestMapping("/changeFlagEnable")
    public ActionResult changeFlagEnable(@RequestParam Integer id) {
        ActionResult ar = new ActionResult();
        try {
            userService.changeUserFlagEnable(id);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }
    @ResponseBody
    @RequestMapping("/changeFlagDisable")
    public ActionResult changeFlagDisable(@RequestParam Integer id) {
        ActionResult ar = new ActionResult();
        try {
           userService.changeUserFlagDisable(id);
            ar.setStatus(Constant.RESPONSE_CODE_SUCCESS);
            ar.setMsg("OK");
        } catch (RequsetErrorException e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(Constant.RESPONSE_CODE_FAIL);
            ar.setMsg(e.getMessage());
        }
        return ar;
    }

}
