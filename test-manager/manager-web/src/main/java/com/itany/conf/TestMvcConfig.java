package com.itany.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TestMvcConfig implements WebMvcConfigurer {

    //添加ViewController
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/showlist").setViewName("userlist");
        registry.addViewController("/showtest").setViewName("list");
        registry.addViewController("/showmanager").setViewName("backend/manager");
        registry.addViewController("/showuser").setViewName("backend/userlist");
        registry.addViewController("/showMain").setViewName("backend/index");
        registry.addViewController("/showsupplierExamine").setViewName("backend/supplier_examine");
        registry.addViewController("/showplatform").setViewName("backend/supplier_platform");
        registry.addViewController("/showsupplier").setViewName("backend/supplierlist");
    }
}
