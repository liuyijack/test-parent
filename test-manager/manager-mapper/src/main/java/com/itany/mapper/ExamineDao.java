package com.itany.mapper;

import com.itany.entity.Examine;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExamineDao {
    void insertExamine(Examine examine);
    List<Examine> selectByCondition(Map<String,Object>map);
    Examine selectById(@Param("id") Integer id);
    void updateById(Examine examine);
}
