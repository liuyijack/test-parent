package com.itany.mapper;

import com.itany.entity.Manager;

import java.util.List;
import java.util.Map;

public interface ManagerDao {

List<Manager> selectByCondition(Map<String,Object> map);

void insertManager(Manager manager);

}



