package com.itany.mapper;

import com.itany.entity.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BookDao {

void insertBook(Book book);
List<Book> selectByCondition(Map<String,Object> map);
Book selectById(@Param("id") Integer id);
void updateById(Book book);
}
