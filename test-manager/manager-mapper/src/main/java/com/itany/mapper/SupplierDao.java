package com.itany.mapper;

import com.itany.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SupplierDao {
    List<Supplier> selectByCondition(Map<String,Object> map);
    void insertsupplier(Supplier supplier);
    void updatesupplier(Supplier supplier);
    Supplier selectById(@Param("id") Integer id);
    void exchangeflagEnable(@Param("id") Integer id);
    void exchangeflagDisable(@Param("id") Integer id);
    Supplier selectByName(@Param("name") String name);

}
