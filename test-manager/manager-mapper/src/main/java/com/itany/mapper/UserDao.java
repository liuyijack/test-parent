package com.itany.mapper;

import com.itany.entity.User;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

public interface UserDao {

List<User> selectByCondition(Map<String,Object> map);

User selectById(@Param("id") Integer id);
void updateById(User user);
List<User> selectByusername(@Param("username") String username);
void exchangeUserDisable(@Param("id") Integer id);

void exchangeUserEnable(@Param("id") Integer id);

}
