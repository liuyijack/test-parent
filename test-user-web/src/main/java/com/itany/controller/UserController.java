package com.itany.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.itany.pojo.User;
import com.itany.user.service.IUserService;
import com.itany.vo.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by tyh on 2018/9/22.
 */
@Controller
@RequestMapping("/userserver")
public class UserController {

    @Reference
    private IUserService userService;



    @RequestMapping("/addUser")
    @ResponseBody
    public ActionResult addUser(User user){
        ActionResult ac= new ActionResult();
        userService.addUser(user);
        ac.setStatus(0);
        return ac;
    }

    @RequestMapping("/findUsers")
    public ModelAndView findUsers(@RequestParam(defaultValue = "1")Integer pageNo,
                                  @RequestParam(defaultValue = "10")Integer pageSize){
        PageInfo<User> pageInfo =  userService.findUsers(pageNo,pageSize);
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("pageInfo",pageInfo);
        return new ModelAndView("userlist",map);
    }


}
