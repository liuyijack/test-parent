package com.itany.utils;

import java.util.UUID;

public class StringUtils {

	public static String rename(String fileName) {
		int lastDot = fileName.lastIndexOf(".");
		String suffix = fileName.substring(lastDot);
		return idGenerate() + suffix;
	}

	public static String idGenerate() {
		return UUID.randomUUID().toString();
	}
}
